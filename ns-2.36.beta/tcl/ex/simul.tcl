# Copyright (c) 1997 Regents of the University of California.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#      This product includes software developed by the Computer Systems
#      Engineering Group at Lawrence Berkeley Laboratory.
# 4. Neither the name of the University nor of the Laboratory may be used
#    to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# YAVISTA SAMPLE NS SCRIPT
# Note : the main parameter to be set is the directory where the trace file is generated (see XML trace file settings section)

# ======================================================================
# Define options
# ======================================================================
set val(chan)           Channel/WirelessChannel    ;# channel type
set val(prop)           Propagation/TwoRayGround   ;# radio-propagation model
#set val(prop)           Propagation/FreeSpace   ;# radio-propagation model
set val(netif)          Phy/WirelessPhy            ;# network interface type
set val(mac)            Mac/802_11                 ;# MAC type
set val(ifq)            Queue/DropTail/PriQueue    ;# interface queue type
set val(ll)             LL                         ;# link layer type
set val(ant)            Antenna/OmniAntenna        ;# antenna model
set val(ifqlen)         1000                       ;# max packet in ifq
#set val(nn)             3                          ;# number of mobilenodes YAVISTA
set val(nn)             10                         ;# number of mobilenodes YAVISTA
#set val(rp)             DumbAgent                  ;# routing protocol YAVISTA
set val(rp)             AODV                      ;# routing protocol YAVISTA
# ======================================================================
# Main Program
# ======================================================================

#############################
# Initialize Global Variables
#############################

#
# these value are set to have with the tworay ground model
# the CSthreshold distance equals to 710.6m 
# the RXthreshol  distance equals to 399.6m

Phy/WirelessPhy set CSThresh_ 7.943e-13
Phy/WirelessPhy set RXThresh_ 7.943e-12
Phy/WirelessPhy set bandwidth_ 2e6
Phy/WirelessPhy set Pt_ 0.04
Phy/WirelessPhy set freq_ 2.4e+9
Phy/WirelessPhy set L_ 1.0  

set ns_		[new Simulator]
#name and path of the trace file using the original ns format; this file is not used
append tracename "/tmp/" "simple.tr"
set tracefd     [open $tracename w]
$ns_ trace-all $tracefd

#set the generator seed
set seed 1
ns-random $seed

set bandwidth 2Mb

#time when the nodes begin to generate packets
set startTrafic 0.0
#duration of the traffic after the nodes have begun to generate packets
set durationTrafic 1

#time when the nodes stop to generate packets (should not be changed)
set stopTrafic [ expr ( $durationTrafic + $startTrafic ) ]
#how many time to wait (after the nodes have stop to generate packets) before exiting
set timeAfterStopTrafic 0
#time of the end of the simulation (should not be changed)
set stopSimulation [ expr ( $stopTrafic + $timeAfterStopTrafic ) ]
#when to begin to commence le debut de la trace
set startXML 0
set durationXML 1
#when does the simualtion ends (should not be changed)
set stopXML [ expr ( $startXML + $durationXML ) ]
#XML trace cannot end after simulation terminaison
if { $stopXML > $stopSimulation } {
    set stopXML $stopSimulation
}


#############################
#Application configuration
#############################

#size of the packets
set P 50 
#time interval between each packet generated
set p 0.1
#RTS/CTS configuration
#RTSalwaysActive is set to 0 if the RTS mechanism is disabled, to 1 otherwise
set RTSAlwaysActive 0

#############################
#XML trace file settings
#############################

#that's the name of the XML file trace to generate, the name contains the main simulation parameters
#some of these parameters are compulsory for YAVISTA to correctly treat the trace file
#you must change the name of the directory according to your own directory tree 
set version [ ns-version ]
append xmlfile "ns" $version "-s" $seed "-N" $val(nn) "-A-debXML" $startXML "-durXML" $durationXML "-db-P" $P "-p" $p "-deb" $startTrafic "-dur" $durationTrafic "-RTSaa" $RTSAlwaysActive "-nr" $bandwidth "-phyGenericCard-.xml" 
append xmlname "/tmp/" $xmlfile

###########################
#Flow Configuration
##########################

#we add 30 to the length of the packet to have the same lengh than glomosim with the same P because glomosim include less header than ns
set P [ expr ($P + 30) ]
#for each flow (a flow is between two nodes) we have a flow configration
#the flow configuration includes the source and destination adreeses and other info like packet time interval....
set flux_src(0) 0 
set flux_dst(0) 1
set flux_period(0) $p
set flux_packetsize(0) $P
set flux_start(0) $startTrafic
set flux_stop(0)  $stopTrafic

set flux_src(1) 0
set flux_dst(1) 7
set flux_period(1) $p
set flux_packetsize(1) [ expr ($P + 0) ]
set flux_start(1) $startTrafic
set flux_stop(1)  $stopTrafic

set flux_src(2) 1
set flux_dst(2) 2
set flux_period(2) $p
set flux_packetsize(2) [ expr ($P + 0) ]
set flux_start(2) $startTrafic
set flux_stop(2)  $stopTrafic

set flux_src(3) 2
set flux_dst(3) 3
set flux_period(3) $p
set flux_packetsize(3) [ expr ($P + 0) ]
set flux_start(3) $startTrafic
set flux_stop(3)  $stopTrafic

set flux_src(4) 3
set flux_dst(4) 4
set flux_period(4) $p
set flux_packetsize(4) [ expr ($P + 0) ]
set flux_start(4) $startTrafic
set flux_stop(4)  $stopTrafic

set flux_src(5) 4
set flux_dst(5) 5
set flux_period(5) $p
set flux_packetsize(5) [ expr ($P + 0) ]
set flux_start(5) $startTrafic
set flux_stop(5)  $stopTrafic

set flux_src(6) 5
set flux_dst(6) 2
set flux_period(6) $p
set flux_packetsize(6) [ expr ($P + 0) ]
set flux_start(6) $startTrafic
set flux_stop(6)  $stopTrafic

set flux_src(7) 5
set flux_dst(7) 0
set flux_period(7) $p
set flux_packetsize(7) [ expr ($P + 0) ]
set flux_start(7) $startTrafic
set flux_stop(7)  $stopTrafic

set flux_src(8) 6
set flux_dst(8) 8
set flux_period(8) $p
set flux_packetsize(8) [ expr ($P + 0) ]
set flux_start(8) $startTrafic
set flux_stop(8)  $stopTrafic

set flux_src(9) 7
set flux_dst(9) 6
set flux_period(9) $p
set flux_packetsize(9) [ expr ($P + 0) ]
set flux_start(9) $startTrafic
set flux_stop(9)  $stopTrafic

set flux_src(10) 9
set flux_dst(10) 5
set flux_period(10) $p
set flux_packetsize(10) [ expr ($P + 0) ]
set flux_start(10) $startTrafic
set flux_stop(10)  $stopTrafic

#if we have X flow configurations, nb_flux equals X
set nb_flux 11

#############################
# Create God and topology 
#############################

# set up topography object
set topo       [new Topography]

$topo load_flatgrid 2000 2000

create-god $val(nn)

#
#  Create the specified number of mobilenodes [$val(nn)] and "attach" them
#  to the channel. 
#  Here two nodes are created : node(0) and node(1)

#############################
# Configure node
#############################

        $ns_ node-config -adhocRouting $val(rp) \
			 -llType $val(ll) \
			 -macType $val(mac) \
			 -ifqType $val(ifq) \
			 -ifqLen $val(ifqlen) \
			 -antType $val(ant) \
			 -propType $val(prop) \
			 -phyType $val(netif) \
			 -channelType $val(chan) \
			 -topoInstance $topo \
			 -agentTrace ON \
			 -routerTrace ON \
			 -macTrace ON \
			 -movementTrace ON			
			 
	for {set i 0} {$i < $val(nn) } {incr i} {
	    set node_($i) [$ns_ node]	
	    $node_($i) random-motion 0		;# disable random motion
            #we use a special mechanism to disabled ARP; ARP is disabled for convenience
	    set arp [$node_($i) getArp]
            for {set j 0} {$j < $val(nn) } {incr j} {
		$arp ip2mac $j $j
	    }
	    set mac_($i) [$node_($i) getMac 0]
	    $mac_($i) set dataRate_ $bandwidth
	    $mac_($i) set basicRate_ $bandwidth
	    if { $RTSAlwaysActive != 0 } {
	       $mac_($i) set RTSThreshold_  0
	    } else {
		$mac_($i) set RTSThreshold_  3000
	    } 
	}

############################
#node movements and positions
############################

#we set Z=0 becasue the antenna heigh is 1.5m (see tcl/lib/ns-default.tcl)
#we Z=0 we finally get the same configuration than glomosim whose antennas are 1.5m heigh

$node_(0) set X_ 200.0
$node_(0) set Y_ 0.0
$node_(0) set Z_ 0

$node_(1) set X_ 0.0
$node_(1) set Y_ 80.0
$node_(1) set Z_ 0

$node_(2) set X_ 300.0
$node_(2) set Y_ 20.0
$node_(2) set Z_ 0

$node_(3) set X_ 700.0
$node_(3) set Y_ 50.0
$node_(3) set Z_ 0

$node_(4) set X_ 400.0
$node_(4) set Y_ 200.0
$node_(4) set Z_ 0

$node_(5) set X_ 500.0
$node_(5) set Y_ 400.0
$node_(5) set Z_ 0

$node_(6) set X_ 300.0
$node_(6) set Y_ 100.0
$node_(6) set Z_ 0

$node_(7) set X_ 800.0
$node_(7) set Y_ 200.0
$node_(7) set Z_ 0

$node_(8) set X_ 1000.0
$node_(8) set Y_ 300.0
$node_(8) set Z_ 0

$node_(9) set X_ 600.0
$node_(9) set Y_ 600.0
$node_(9) set Z_ 0



#do we need some movements? if yes set the following parameters
#$ns_ at 0.119 "$node_(1) set  X_ 190.0"
#$ns_ at 0.119 "$node_(2) set  X_ 380.0"
#$ns_ at 0.119 "$node_(3) set  X_ 570.0"
#$ns_ at 0.119 "$node_(4) set  X_ 760.0"
#$ns_ at 0.119 "$node_(5) set  X_ 950.0"


############################
#Automatic simulation execution; no more parametres should be changed below this point
############################

#Application automatic installation (should not be changed)

for {set i 0} {$i < $nb_flux } {incr i} {

    set src $flux_src($i)
    set dst $flux_dst($i)
    set period $flux_period($i)
    set size $flux_packetsize($i)
    
    #A CBR Agent is used for each flow
    set udp($src) [new Agent/UDP]
    $ns_ attach-agent $node_($src) $udp($src)
    set cbr($src) [new Application/Traffic/CBR]
    $cbr($src) attach-agent $udp($src)
    $udp($src) set class_ 0
    $cbr($src) set packetSize_ $size
    $cbr($src) set interval_ $period
	
    set null($dst) [new Agent/Null]
    $ns_ attach-agent $node_($dst) $null($dst)
    
    $ns_ connect $udp($src) $null($dst)    
}


# Tell nodes when the simulation shoul starts and ends (should not be changed)
for {set i 0} {$i < $val(nn) } {incr i} {
    $ns_ at $startTrafic "$node_($i) reset";
}

for {set i 0} {$i < $nb_flux } {incr i} {

    set src $flux_src($i)
    $ns_ at $flux_start($i) "$cbr($src) start"
}

for {set i 0} {$i < $nb_flux } {incr i} {

    set src $flux_src($i)
    $ns_ at $flux_stop($i) "$cbr($src) stop"
}

$ns_ at $startXML "startXML"
$ns_ at $stopXML "stopXML"

$ns_ at $stopSimulation "stop"
$ns_ at $stopSimulation "puts \"NS EXITING...\" ; $ns_ halt"

#Misc Routines
proc startXML {} {
    global xmlname node_ val
    set traces    [open $xmlname w]
    puts $traces "<?xml version='1.0'?>"
    puts $traces "<trace xmlns:mac='http://example.org/mac' xmlns:phy='http://example.org/phy' xmlns:mob='http://example.org/mob'>"
    close $traces
    for {set i 0} {$i < $val(nn) } {incr i} {	
	    set mac_($i) [$node_($i) getMac 0]
	    $mac_($i) xmlstart $xmlname
    }
    puts "StartXML"
}

proc stopXML {} {
    global xmlname node_ val

    for {set i 0} {$i < $val(nn) } {incr i} {    
	    set mac_($i) [$node_($i) getMac 0]
	    $mac_($i) xmlstop $xmlname
    }
    puts "StopXML"
}

proc stop {} {
    global ns_ tracefd xmlname node_ val
    
    for {set i 0} {$i < $val(nn) } {incr i} {    
	    set mac_($i) [$node_($i) getMac 0]
	    $mac_($i) xmlclose $xmlname
    }

    set traces    [open $xmlname a]
    puts $traces "\n</trace>"
    close $traces

    $ns_ flush-trace
    close $tracefd
    puts "Stop"
}

#start simulation
puts "Starting Simulation; waiting for the XML trace file to be generated..."
$ns_ run
